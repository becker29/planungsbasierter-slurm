\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einführung}{7}{section.1}%
\contentsline {section}{\numberline {2}Grundlagen}{8}{section.2}%
\contentsline {subsection}{\numberline {2.1}Komponenten von SLURM}{8}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}SLURM/SPANK Plugin}{9}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Grundstruktur SLURM Cluster}{9}{subsection.2.3}%
\contentsline {section}{\numberline {3}Planbasiertes Scheduling Plugin für SLURM}{9}{section.3}%
\contentsline {subsection}{\numberline {3.1}Recherche}{10}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Entwurf - Projektstruktur}{11}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Projektstruktur und Testumgebung}{12}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Entwurf - Kommunikation, Interface und SPANK}{14}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Entwicklung - Interface und Parameterannahme}{15}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Entwurf - Implementierung der Plugins}{17}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Entwicklung - Programmlogik}{18}{subsection.3.7}%
\contentsline {subsubsection}{\numberline {3.7.1}Jobannahme}{18}{subsubsection.3.7.1}%
\contentsline {subsubsection}{\numberline {3.7.2}Planvalidierung}{19}{subsubsection.3.7.2}%
\contentsline {subsubsection}{\numberline {3.7.3}Jobübergabe}{19}{subsubsection.3.7.3}%
\contentsline {subsubsection}{\numberline {3.7.4}Jobabarbeitung}{20}{subsubsection.3.7.4}%
\contentsline {subsection}{\numberline {3.8}Resultate}{21}{subsection.3.8}%
\contentsline {subsubsection}{\numberline {3.8.1}Plugins}{21}{subsubsection.3.8.1}%
\contentsline {subsubsection}{\numberline {3.8.2}Interface}{22}{subsubsection.3.8.2}%
\contentsline {section}{\numberline {4}Konklusion}{23}{section.4}%
\contentsline {section}{Literaturverzeichnis}{23}{section*.3}%
