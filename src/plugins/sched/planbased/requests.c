#include "requests.h"

#include <json-c/json.h>
#include <string.h>

Request* Request_Get(const char* request_data, unsigned int request_data_len)
{
    Request* req = Request_Create();
    req->json_content        = request_data;
    req->json_content_length = request_data_len;

    struct json_object* json_str = json_tokener_parse(req->json_content);
    const char* type_str = json_object_get_string(json_object_object_get(json_str, "request"));

    //switch type
    if (type_str == NULL)
    {
        req->type = REQUEST_END;
    }
    else if (strcmp(type_str, "status") == 0)
    {
        req->type = REQUEST_STATUS;
        req->status = (enum request_status)json_object_get_int(json_object_object_get(json_str, "status"));
        req->job_id = json_object_get_int64(json_object_object_get(json_str, "JobID"));
    }
    else if (strcmp(type_str, "action") == 0)
    {
        req->type = REQUEST_ACTION;
        req->action = (enum request_action)json_object_get_int(json_object_object_get(json_str, "action"));
        req->job_id = json_object_get_int64(json_object_object_get(json_str, "JobID"));
    }
    else if (strcmp(type_str, "plan") == 0)
    {
        req->type = REQUEST_PLAN;
        const char* plan = json_object_get_string(json_object_object_get(json_str, "plan"));
        if (plan == NULL)
            req->type = REQUEST_END;
        else
        {
            req->plan = (char*)malloc(strlen(plan) + 1);
            strcpy(req->plan, plan);
        }
    }
    else
    {
        req->type = REQUEST_END; //Request type invalid
    }

    json_object_put(json_str);
    return req;
}

Request* Request_Create()
{
    Request* req = malloc(sizeof(Request));
    req->action = ACTION_END;
    req->job_id = 0;
    req->json_content = NULL;
    req->json_content_length = 0;
    req->plan = NULL;
    req->status = STATUS_END;
    req->type = REQUEST_END;
    return req;
}

void Request_Generate(Request* request)
{
    if (request->json_content != NULL)
        free(request->json_content);
    
    struct json_object* json_str = json_object_new_object();

    //switch request type
    if (request->type == REQUEST_STATUS)
    {
        json_object_object_add(json_str, "request", json_object_new_string("status"));
        json_object_object_add(json_str, "status", json_object_new_int((int)(request->status)));
        json_object_object_add(json_str, "JobID", json_object_new_int64(request->job_id));
    }
    else if (request->type == REQUEST_ACTION)
    {
        json_object_object_add(json_str, "request", json_object_new_string("action"));
        json_object_object_add(json_str, "action", json_object_new_int((int)(request->action)));
        json_object_object_add(json_str, "JobID", json_object_new_int64(request->job_id));
    }
    else if (request->type == REQUEST_PLAN)
    {
        json_object_object_add(json_str, "request", json_object_new_string("plan"));
        if (request->plan != NULL)
        {
            json_object_object_add(json_str, "plan", json_object_new_string(request->plan));
        }
    }
    else
    {
        //invalid type
        request->json_content = NULL;
        request->json_content_length = 0;
        json_object_put(json_str);
        return;
    }

    const char* json_content = json_object_to_json_string(json_str);

    request->json_content = (char*)malloc(strlen(json_content) + 1);
    strcpy(request->json_content, json_content);
    request->json_content_length = strlen(request->json_content);
    json_object_put(json_str);
    return;
}

/**
 * @brief Frees all data in the Request Object including the json_content.
 * 
 * @param request Request Object
 */
void Request_Destroy(Request* request)
{
    if (request->json_content != NULL)
        free(request->json_content);
    
    if (request->plan != NULL)
        free(request->plan);
    
    free(request);
    return;
}