/**
 * @file requests.h
 * @author René Pascal Becker (becker29@zedat.fu-berlin.de)
 * @brief Header file for the requests that get sent and received
 * @version 0.1
 * @date 2020-10-01
 * 
 * @copyright Copyright (c) 2020
 * 
 * It is given by the context if a request is meant to be a question or an answer.
 * So this file would have a much more appropriate name if it were called "msg.h".
 */

#ifndef REQUESTS_H
#define REQUESTS_H

#include "connection.h"

/**
 * @brief All types a Request can have.
 * 
 */
enum request_type {
    REQUEST_ACTION,
	REQUEST_STATUS,
    REQUEST_PLAN,
    REQUEST_END, //end of types
};

/**
 * @brief Status codes used for request_type REQUEST_STATUS. 
 * 
 */
enum request_status {
    STATUS_VALID,
    STATUS_INVALID,
    STATUS_ALIVE,
    STATUS_JOB_PENDING,
    STATUS_JOB_RUNNING,
    STATUS_JOB_CANCELLED,
    STATUS_JOB_COMPLETE,
    STATUS_JOB_UNDEFINED,
    STATUS_END, //end of status
};

/**
 * @brief Action codes used for request_type REQUEST_ACTION.
 * 
 */
enum request_action {
    ACTION_RUN_JOB,
    ACTION_ABORT_JOB,
    ACTION_END, //end of actions
};

/**
 * @brief 
 *  Request Object used to parse requests for the PSPS Plugin and the planbased scheduling.
 *  More appropriate name would be "Msg" because it can be a question or an answer depending
 *  on the context of where it is used.
 */
struct Request
{
    char*                   json_content;
    unsigned int            json_content_length;
    enum request_type       type; //type of the request

    enum request_status     status; //set for REQUEST_STATUS
    enum request_action     action; //set for REQUEST_ACTION
    long                    job_id; //set for REQUEST_STATUS, REQUEST_ACTION
    const char*             plan;   //set for REQUEST_PLAN
};
typedef struct Request Request;

/**
 * @brief 
 * Parses incoming data into a Request object.
 * Be aware that the Request object takes ownership of the request_data
 * It will be freed when using Request_Destroy
 * 
 * To prevent that -> set json_content to NULL prior to calling Request_Destroy
 * 
 * @param request_data Data of the request
 * @param request_data_len Length of that data
 * @return Request* Generated Request object
 */
Request* Request_Get(const char* request_data, unsigned int request_data_len);

/**
 * @brief Creates an empty Request object
 * 
 * @return Request* Empty Request
 */
Request* Request_Create();

/**
 * @brief Generates a json_content appropriate to the set Variables
 * in the Request object. Be aware that this function will free previous data.
 * @param request Request Object
 */
void Request_Generate(Request* request);

/**
 * @brief Frees all data in the Request Object including the json_content.
 * 
 * @param request Request Object
 */
void Request_Destroy(Request* request);


#endif /* REQUESTS_H */