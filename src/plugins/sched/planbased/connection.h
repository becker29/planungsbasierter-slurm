/**
 * @file connection.h
 * @author René Pascal Becker (becker29@zedat.fu-berlin.de)
 * @brief Connection Object for sending/receiving from Third Party Program
 * @version 0.1
 * @date 2020-10-01
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CONNECTION_H
#define CONNECTION_H

struct Connection
{
    int socket_fd;
    int alive;
    const char* host_ip;
    unsigned short port;
};
typedef struct Connection Connection;

/**
 * @brief Initializes a connection and allocate memory for it.
 * 
 * @param host Host to connect to
 * @param port Port to connect to
 * @return Connection* Pointer to Connection. Check for the alive flag
 */
Connection* Connection_Initialize(const char* host, const char* port);

/**
 * @brief Destroys the connection. Will also free the allocated memory!
 * 
 * @param c 
 */
void Connection_Destroy(Connection* c);

/**
 * @brief Receives a Message
 * 
 * @param c Connection Object
 * @param msg_len Length of message received
 * @return char* Message
 */
char* Connection_Blocked_Read(Connection* c, unsigned int* msg_len);

/**
 * @brief Receive Bytes from the Socket buffer. Will read up to buffer_size Bytes.
 * 
 * @param c Connection Object
 * @param buffer Buffer to be written to
 * @param buffer_size Size of the buffer to be written to
 * @return int Count of Bytes received
 */
int Connection_Try_Read(Connection* c, char* buffer, unsigned int buffer_size);

/**
 * @brief Send data to the host
 * 
 * @param c Connection object
 * @param data Data to be sent
 * @param data_size Size of the data to be sent
 * @return int Count of Bytes sent
 */
int Connection_Send(Connection* c, const char* data, unsigned int data_size);

#endif /* CONNECTION_H */