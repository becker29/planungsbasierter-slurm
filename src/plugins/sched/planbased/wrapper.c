#include <stdio.h>

#include "slurm/slurm_errno.h"

#include "src/common/plugin.h"
#include "src/common/log.h"
#include "src/common/node_select.h"
#include "src/common/slurm_priority.h"
#include "src/slurmctld/job_scheduler.h"
#include "src/slurmctld/reservation.h"
#include "src/slurmctld/slurmctld.h"
#include "planbased.h"

const char		plugin_name[]	= "Planbased Scheduler Plugin for SLURM";
const char		plugin_type[]	= "sched/planbased";
const uint32_t	plugin_version	= SLURM_VERSION_NUMBER;

static pthread_t thread = 0;
static pthread_mutex_t thread_flag_mutex = PTHREAD_MUTEX_INITIALIZER;

int init(void)
{
	sched_verbose("Planbased Scheduler plugin initializing...");

	slurm_mutex_lock( &thread_flag_mutex );
	if (thread) {
		debug2( "Planbased Scheduler thread already running, not starting another." );
		slurm_mutex_unlock( &thread_flag_mutex );
		return SLURM_ERROR;
	}

	slurm_thread_create(&thread, planbased_thread, NULL);
	slurm_mutex_unlock( &thread_flag_mutex );
	sched_verbose("Planbased Scheduler plugin successfully initialized...");
	return SLURM_SUCCESS;
}

void fini(void)
{
	verbose( "Planbased Scheduler plugin shutting down." );
	slurm_mutex_lock( &thread_flag_mutex );
	if (thread)
	{
		psps_thread_stop();
		pthread_join(thread, NULL);
		thread = 0;
	}
	slurm_mutex_unlock( &thread_flag_mutex );
}

int slurm_sched_p_reconfig(void)
{
	return SLURM_SUCCESS;
}

uint32_t slurm_sched_p_initial_priority(uint32_t last_prio, struct job_record *job_ptr)
{
	return 0; //prio of 0 -> don't start job
}