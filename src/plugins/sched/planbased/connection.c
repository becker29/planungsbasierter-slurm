#include "connection.h"
#include "slurm/slurm_errno.h"

#include "src/common/log.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <malloc.h>

/*
Based on https://man7.org/linux/man-pages/man3/getaddrinfo.3.html
*/
Connection* Connection_Initialize(const char* host, const char* port)
{
    //Init with default values
    Connection* c = (Connection*)malloc(sizeof(Connection));
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    c->alive = 0;
    c->host_ip = host;
    sscanf(port, "%hd", &c->port);
    c->socket_fd = 0;
    memset(&hints, 0 ,sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM; //opting for reliable sequenced socket type
    hints.ai_flags = 0;
    hints.ai_protocol = IPPROTO_TCP;

    int error_code = getaddrinfo(host, port, &hints, &result);
    if (error_code != 0)
    {
        sched_verbose(gai_strerror(error_code));
        return c;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        c->socket_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        
        if (c->socket_fd == -1)
            continue;

        if (connect(c->socket_fd, rp->ai_addr, rp->ai_addrlen) == 0)
            break; //successfully connected
        
        close(c->socket_fd); //close socket when we cannot connect
    }

    freeaddrinfo(result); //free result

    if (rp == NULL)
    {
        sched_verbose("Could not connect to host. Host not available or host/port invalid.");
        return c;
    }

    c->alive = 1;

    return c;
}

void Connection_Destroy(Connection* c)
{
    if (c->alive)
        close(c->socket_fd);
    
    free(c); //free memmory
    return;
}

char* Connection_Blocked_Read(Connection* c, unsigned int* msg_len)
{
    if (c->alive)
    {
        if (recv(c->socket_fd, (char*)msg_len, sizeof(unsigned int), MSG_WAITALL) < 0)
        {
            c->alive = false;
            *msg_len = 0;
            return NULL;
        }
        if (*msg_len == 0)
            return NULL;
        char* buffer = (char*)malloc((*msg_len) + 1);
        buffer[*msg_len] = 0;
        (int)recv(c->socket_fd, (void*)buffer, *msg_len, MSG_WAITALL);
        return buffer;
    }
    return NULL;
}

int Connection_Try_Read(Connection* c, char* buffer, unsigned int buffer_size)
{
    if (c->alive)
        return (int)recv(c->socket_fd, (void*)buffer, buffer_size, MSG_DONTWAIT);
    return 0;
}

int Connection_Send(Connection* c, const char* data, unsigned int data_size)
{
    if (c->alive)
    {
        if (send(c->socket_fd, (char*)&data_size, sizeof(unsigned int), 0) < 0)
        {
            c->alive = false;
        }
        return (int)send(c->socket_fd, data, data_size, 0);
    }
    return 0;
}