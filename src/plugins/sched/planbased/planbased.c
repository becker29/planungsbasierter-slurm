#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "planbased.h"

#include "slurm/slurm.h"
#include "slurm/slurm_errno.h"
#include "src/common/log.h"

#include "src/slurmctld/burst_buffer.h"
#include "src/slurmctld/fed_mgr.h"
#include "src/slurmctld/job_scheduler.h"
#include "src/slurmctld/locks.h"
#include "src/slurmctld/node_scheduler.h"
#include "src/slurmctld/preempt.h"
#include "src/slurmctld/reservation.h"
#include "src/slurmctld/slurmctld.h"
#include "src/slurmctld/srun_comm.h"

#include <json-c/json.h>
#include <sys/ioctl.h>

#include "requests.h"

//------local variables------
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static bool stop = false;
static const char* host = "std.strangled.net";
static const char* host_port = "25566";
static uint32_t schedule_cycle_duration = 10;
//---------------------------

int get_allocation_service_id_env(char** env, uint32_t env_size, /* OUT */ long* id)
{
	char* id_env = getenvp(env, "PSPS_JOB_ID");

	if (sscanf(id_env, "%ld", id) == 1)
		return 0; //success
	
	return 1; //could not find env var
}

bool schedule_job(long job_id)
{
	job_record_t* job_ptr;
	ListIterator it = list_iterator_create(job_list);

	debug2("Received schedule instruction for JobID %Ld", job_id);

	//iterate through all jobs
	while ((job_ptr = list_next(it)))
	{
		//is this job queued for us to check?
		if (job_ptr->job_state == JOB_PENDING && job_ptr->priority == 0)
		{
			uint32_t env_size;
			long allocation_service_id;
			char** env = get_job_env(job_ptr, &env_size);

			if (get_allocation_service_id_env(env, env_size, &allocation_service_id) > 0)
			{
				//invalid environment
				debug2("Could not find Allocation Service ID in environment variables of Job %u with Allocation ID %Ld", job_ptr->job_id, job_id);
				continue;
			}

			if (allocation_service_id == job_id)
			{
				//found job to be executed -> schedule it
				job_ptr->priority = 1;
				job_ptr->state_reason = WAIT_NO_REASON;
				list_iterator_destroy(it);
				return true;
			}
		}
	}
	list_iterator_destroy(it);
	return false;
}

bool cancel_job(long job_id)
{
	job_record_t* job_ptr;
	ListIterator it = list_iterator_create(job_list);

	debug2("Received cancel instruction for JobID %Ld", job_id);

	//iterate through all jobs
	while ((job_ptr = list_next(it)))
	{
		//is this job running?
		if (job_ptr->job_state == JOB_RUNNING)
		{
			uint32_t env_size;
			long allocation_service_id;
			char** env = get_job_env(job_ptr, &env_size);

			if (get_allocation_service_id_env(env, env_size, &allocation_service_id) > 0)
			{
				//invalid environment
				debug2("Could not find Allocation Service ID in environment variables of Job %u with Allocation ID %Ld", job_ptr->job_id, job_id);
				continue;
			}

			if (allocation_service_id == job_id)
			{
				//found job to be cancelled
				list_iterator_destroy(it);
				return job_signal(job_ptr, SIGKILL, KILL_JOB_ARRAY, 0, false) == 0; //signal a kill of the job
			}
		}
	}
	list_iterator_destroy(it);
	return false;
}

job_record_t* get_job(long job_id)
{
	job_record_t* job_ptr;
	ListIterator it = list_iterator_create(job_list);

	//iterate through all jobs
	while ((job_ptr = list_next(it)))
	{
		uint32_t env_size;
		long allocation_service_id;
		char** env = get_job_env(job_ptr, &env_size);

		if (get_allocation_service_id_env(env, env_size, &allocation_service_id) > 0)
			continue;

		if (allocation_service_id == job_id)
		{
			//found job
			list_iterator_destroy(it);
			return job_ptr;
		}
	}
	list_iterator_destroy(it);
	return NULL;
}

extern void* planbased_thread(void *args)
{
	//Initialize/reset values
	Connection* c = Connection_Initialize(host, host_port);
	stop = false;
	slurmctld_lock_t all_locks = { READ_LOCK, WRITE_LOCK, READ_LOCK, READ_LOCK, READ_LOCK };

	if (!c->alive)
		debug2("Could not connect to Allocation Service!");
	debug2("Initialized scheduler thread.");

	while (1)
	{
		//check if we haven't been stopped
		slurm_mutex_lock(&lock);
		if (stop)
		{
			slurm_mutex_unlock(&lock);
			break;
		}
		slurm_mutex_unlock(&lock);

		//Wait and check for requests
		sleep(schedule_cycle_duration);
		if (c->alive)
		{
			int bytes_available;
			if (ioctl(c->socket_fd, FIONREAD, &bytes_available) < 0)
			{
				c->alive = false;
			}

			if (bytes_available > 0)
			{
				//Get Request and process it
				unsigned int msg_len = 0;
				long job_id;
				const char* buffer = Connection_Blocked_Read(c, &msg_len);

				Request* req = Request_Get(buffer, msg_len + 1); // +1 for the 0 terminator

				if (req->type == REQUEST_ACTION) //handle actions
				{
					job_id = req->job_id;

					if (req->action == ACTION_RUN_JOB)
					{
						lock_slurmctld(all_locks);
						bool rc = schedule_job(job_id);
						unlock_slurmctld(all_locks);

						if (rc)
						{
							if (schedule(0) == 0)
								debug2("Job could not get scheduled."); //Job did not get scheduled -> error handling
						}
						else
							debug2("Could not find the job."); //could not find job -> return error to acc service
					}
					else if (req->action == ACTION_ABORT_JOB)
					{
						lock_slurmctld(all_locks);
						bool rc = cancel_job(job_id);
						unlock_slurmctld(all_locks);
					}
				}
				else if (req->type == REQUEST_STATUS)
				{
					Request* response = Request_Create();
					response->type = REQUEST_STATUS;
					response->job_id = req->job_id;

					lock_slurmctld(all_locks);
					//The Allocation Service wants to know the status of a Job
					job_record_t* job_ptr = get_job(req->job_id);

					//map job state to job status
					if (job_ptr == NULL)
						response->status = STATUS_INVALID;
					else if (job_ptr->job_state == JOB_PENDING)
						response->status = STATUS_JOB_PENDING;
					else if (job_ptr->job_state == JOB_RUNNING)
						response->status = STATUS_JOB_RUNNING;
					else if (job_ptr->job_state == JOB_CANCELLED)
						response->status = STATUS_JOB_CANCELLED;
					else if (job_ptr->job_state == JOB_COMPLETE)
						response->status = STATUS_JOB_COMPLETE;
					else
						response->status = STATUS_JOB_UNDEFINED;

					unlock_slurmctld(all_locks);

					Request_Generate(response);
					Connection_Send(c, response->json_content, response->json_content_length);
					Request_Destroy(response);
				}

				Request_Destroy(req);
			}

			//Send heartbeat
			Request* req = Request_Create();
			req->type = REQUEST_STATUS;
			req->status = STATUS_ALIVE;
			Request_Generate(req);
			Connection_Send(c, req->json_content, req->json_content_length);
			Request_Destroy(req);
		}
		else
		{
			debug2("Scheduler lost connection to Allocation Service. Trying to reconnect...");
			Connection_Destroy(c);
			c = Connection_Initialize(host, host_port);
		}
	}
	Connection_Destroy(c);
	return NULL;
}

extern void planbased_thread_stop(void)
{
	slurm_mutex_lock(&lock);
	stop = true;
	slurm_mutex_unlock(&lock);
	return;
}