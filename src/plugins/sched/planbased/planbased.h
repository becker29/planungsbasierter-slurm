/**
 * @file planbased.h
 * @author René Pascal Becker (becker29@zedat.fu-berlin.de)
 * @brief Main header file for the Planbased Scheduling Plugin for SLURM
 * @version 0.1
 * @date 2020-08-06
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef PLANBASED_H
#define PLANBASED_H

/**
 * @brief Starts a thread that periodically checks if there is a job that needs to be run.
 * 
 * @param args arguments (nothing specific yet)
 * @return void* pointer (nothing specific yet)
 */
extern void *planbased_thread(void *args);

/**
 * @brief Stops the psps thread
 * 
 */
extern void planbased_thread_stop(void);

#endif	/* PLANBASED_H */