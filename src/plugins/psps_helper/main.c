#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/resource.h>

#include <slurm/spank.h>
//Quick and Dirty for now
#include "../src/plugins/sched/planbased/connection.h"
#include "../src/plugins/sched/planbased/connection.c"
#include "../src/plugins/sched/planbased/requests.h"
#include "../src/plugins/sched/planbased/requests.c"

/*
 * Must define this Macro for the Slurm Plugin Loader.
 */
SPANK_PLUGIN(psps_helper, 1);

#define PRIO_ENV_VAR "SLURM_PSPS_HELPER"
#define PRIO_NOT_SET 42

static const char* host = "std.strangled.net";
static const char* host_port = "25565";

static int psps_plan_opt_cb (int val, const char *optarg, int remote);

struct spank_option plan_opt = {
      .name = "plan",
      .arginfo = "[starttime;endtime]",
      .usage = "Plan for the job (start and end time)",
      .has_arg = 2,
      .val = 0,
      .cb = (spank_opt_cb_f)psps_plan_opt_cb
};

/*
 *  Called from both srun and slurmd.
 */
int slurm_spank_init (spank_t sp, int ac, char **av)
{
    if (spank_context () == S_CTX_ALLOCATOR)
    {
        spank_option_register(sp, &plan_opt);
    }
    return (0);
}

static int psps_plan_opt_cb (int val, const char *optarg, int remote)
{
    if (optarg == NULL)
    {
        slurm_error("planbased-scheduling: No plan given!");
    }
    else
    {
        Connection* c = Connection_Initialize(host, host_port);

        if (!c->alive)
        {
            slurm_error("planbased-scheduling: Could not connect to the Allocation Service.");
        }
        else
        {
            //Send plan
            Request* req = Request_Create();
            req->type = REQUEST_PLAN;
            req->plan = optarg;
            Request_Generate(req);
            Connection_Send(c, req->json_content, req->json_content_length);
            req->plan = NULL; //prevent optarg getting deleted
            Request_Destroy(req);
            
            slurm_info("planbased_scheduling: Sent plan to the Allocation Service. Waiting for a response...");

            int len = 0;
            long jobID = 0;
            char* buffer = Connection_Blocked_Read(c, &len);
            
            Connection_Destroy(c);

            req = Request_Get(buffer, len);

            if (req->type == REQUEST_STATUS)
            {
                if (req->status == STATUS_VALID)
                {
                    jobID = req->job_id;
                    slurm_info("planbased_scheduling: Validated plan. Job has been scheduled.");
                    //convert Job ID to string and set it as an env variable for use later on (in the scheduler)
                    char str[64] = { 0 };
                    sprintf(str, "%Ld", jobID);
                    setenv("PSPS_JOB_ID", str, 1);
                    slurm_info("planbased_scheduling: Job ID for scheduling is: %Ld", jobID);
                    return 0;
                }
                else if (req->status == STATUS_INVALID)
                {
                    slurm_error("planbased_scheduling: Plan has not been accepted. Invalid Datetime or timeslot taken.");
                    return -1;
                }
            }
            else
            {
                slurm_info("planbased_scheduling: Received invalid response from the Allocation Service.");
            }

            Request_Destroy(req);
        }
    }
    return -1;
}