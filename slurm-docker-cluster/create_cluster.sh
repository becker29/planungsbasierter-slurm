#!/bin/bash
rsync -av --progress ../../slurm ./ --exclude slurm-docker-cluster --exclude .git
docker build -t slurm-docker-cluster:19.05.1 .
docker-compose up -d
sleep 15
./register_cluster.sh