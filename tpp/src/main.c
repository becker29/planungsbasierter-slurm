#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

#include <json-c/json.h>

#define BUF_SIZE 500

int main(int argc, char** argv)
{
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int sfd, s;
	struct sockaddr_storage peer_addr;
	socklen_t peer_addr_len;
	ssize_t nread;
	char buf[BUF_SIZE];

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	s = getaddrinfo(NULL, argv[1], &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		exit(EXIT_FAILURE);
	}

	/* getaddrinfo() returns a list of address structures.
		Try each address until we successfully bind(2).
		If socket(2) (or bind(2)) fails, we (close the socket
		and) try the next address. */

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype,
				rp->ai_protocol);
		if (sfd == -1)
			continue;

		if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
			break;                  /* Success */

		close(sfd);
	}

	if (rp == NULL) {               /* No address succeeded */
		fprintf(stderr, "Could not bind\n");
		exit(EXIT_FAILURE);
	}

	freeaddrinfo(result);           /* No longer needed */

	printf("Listening...\n");
	listen(sfd, 1);

	for (;;) {
		int clfd = accept(sfd, NULL, NULL);
		printf("New Connection\n");
		if (clfd < 0)
		{
			sleep(1);
		}
		else
		{
			for (;;)
			{
				unsigned int msg_len = 0;
				recv(clfd, (char*)&msg_len, sizeof(unsigned int), MSG_WAITALL);
				printf("NumBytes: %u\n", msg_len);
				if (msg_len == 0)
					break;
				char* buffer = (char*)malloc(msg_len + 1);
				buffer[msg_len] = '\0';
				recv(clfd, (void*)buffer, msg_len, MSG_WAITALL);
				printf(buffer);
				printf("\n");

				struct json_object* json_str = json_tokener_parse(buffer);
				printf("JSON Parsed...\n");
				char* request_type = json_object_get_string(json_object_object_get(json_str, "request"));
				printf("Request retrieved...\n");
				printf("RequestType: %s\n", request_type);
				if (strcmp(request_type, "plan") == 0)
				{
					struct json_object* json_str2 = json_object_new_object();
					json_object_object_add(json_str2, "request", json_object_new_string("status"));
					json_object_object_add(json_str2, "status", json_object_new_int(0));
					json_object_object_add(json_str2, "JobID", json_object_new_int64(3281L));

					const char* request = json_object_to_json_string(json_str2);
					unsigned int len = strlen(request);
					send(clfd, (char*)&len, sizeof(unsigned int), 0);
					printf("Sent NumBytes: %u\n", len);
					send(clfd, request, strlen(request), 0);
					printf("Sent Request: %s\n", request);

					json_object_put(json_str2);
				}
				else
				{
					struct json_object* json_str2 = json_object_new_object();
					json_object_object_add(json_str2, "request", json_object_new_string("action"));
					json_object_object_add(json_str2, "action", json_object_new_int(0));
					json_object_object_add(json_str2, "JobID", json_object_new_int64(3281L));

					const char* request = json_object_to_json_string(json_str2);
					unsigned int len = strlen(request);
					send(clfd, (char*)&len, sizeof(unsigned int), 0);
					printf("Sent NumBytes: %u\n", len);
					send(clfd, request, strlen(request), 0);
					printf("Sent Request: %s\n", request);

					json_object_put(json_str2);
				}

				printf("Freeing Ressources...\n");
				json_object_put(json_str);
				free(buffer);
			}
		}
		printf("Closed connection\n");
		close(clfd);
	}
}